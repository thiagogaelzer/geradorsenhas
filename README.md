## Descrição do Projeto 

Projeto de uma aplicação para gerar senhas seguras. O gerador usa um algoritmo de combinação aleatória de letras, números e caracteres especiais. 

As senhas geradas podem ter de 4 a 32 caracteres, conforme valor selecionado pelo usuário na interface de geração de senhas.

## Versão do Projeto 

1.2.0

## Execução do Projeto 

Para executar o gerador de senhas, baixe o projeto e abra o arquivo index.html em um navegador web.

## Edição do Projeto 

Para editar os códigos, baixe o projeto e abra-o em uma IDE compatível com as linguagens HTML5, CSS3 e JavaScript. Recomendo usar o Visual Studio Code.

## Desenvolvedor Responśavel

Desenvolvido por Thiago Pereira Gaelzer.
Email: <thiagogaelzer@outlook.com>
Instagram: [@thiagogaelzer]
Linkedin: [Thiago]

[@thiagogaelzer]: <https://www.instagram.com/thiagogaelzer/>
[Thiago]: <https://www.linkedin.com/in/thiago-gaelzer-0bb496320>

Brasil - Novembro de 2024