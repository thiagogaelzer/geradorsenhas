document.addEventListener('DOMContentLoaded', function() {
    let navbarDiv = document.getElementById('navbar');

    fetch('components/navbar.html')
        .then(function(response) {
            return response.text();
        })
        .then(function(data) {
            navbarDiv.innerHTML = data;
        })
        .catch(function(error) {
            console.log('Erro ao carregar o arquivo navbar.html', error);
        });
});
