function gerarSenha() {

  let letrasMinusculas = "abcdefghijklmnopqrstuvwxyz";
  let letrasMaiusculas = "ABCDEFGHIJLMNOPQRSTUVWXYZ";
  let numeros = "0123456789";
  let caracteresEspeciais = "!@#$%^&*()+?><:{}[]";

  let caracteres = "";
  if (document.getElementById("uppercase").checked) caracteres += letrasMaiusculas;
  if (document.getElementById("lowercase").checked) caracteres += letrasMinusculas;
  if (document.getElementById("numbers").checked) caracteres += numeros;
  if (document.getElementById("special").checked) caracteres += caracteresEspeciais;

  let tamanhoSenha = document.querySelector("#qtd-caracteres").value;
  let senha = "";

  if (caracteres === "") {
    mostrarSnackbar("Selecione ao menos um tipo de caractere para gerar a senha!", "warning");
    return;
  }

  for (let i = 0; i < tamanhoSenha; i++) {
    let randomNumber = Math.floor(Math.random() * caracteres.length);
    senha += caracteres.substring(randomNumber, randomNumber + 1);
  }
  document.getElementById("campo-gerador-senha").value = senha
}