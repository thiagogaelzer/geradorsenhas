const value = document.querySelector("#qtd-caracteres");
const input = document.querySelector("#seletor-quantidade-caracteres");
value.textContent = input.value;
input.addEventListener("input", (event) => {
  value.textContent = event.target.value;
});