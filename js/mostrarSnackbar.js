function mostrarSnackbar(mensagem, tipo) {
    let snackbar = document.getElementById("snackbar");
    let snackbarText = document.getElementById("snackbar-text");
    
    // Define o texto da snackbar
    snackbarText.textContent = mensagem;
    
    // Exibe a snackbar
    snackbar.className = "show";

    // Adiciona a classe baseada no tipo da mensagem
    snackbar.classList.add(tipo);

     // Adiciona a classe que exibe a snackbar
    snackbar.classList.add("show");
    
    // Remove a classe "show" após 3 segundos para retirar a snackbar da tela
    setTimeout(function () {
        snackbar.className = snackbar.className.replace("show", "");
        if (tipo) {
            snackbar.classList.remove(tipo);
        }
    }, 3000);
}