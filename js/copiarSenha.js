function copiarSenha() {
    const senhaCopiada = document.getElementById("campo-gerador-senha");

    if (senhaCopiada.value !== "") {
        //Trecho que faz a cópia da senha
        senhaCopiada.select();
        document.execCommand("copy");

        //Trecho que exibe a SnackBar
        mostrarSnackbar("Senha copiada!", "success");
    } 
}
