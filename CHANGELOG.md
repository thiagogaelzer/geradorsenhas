# 1.2.0 (24/11/2024)
Inserção dos seletores de letras maiúsculas, minúsculas, números ou caracteres especiais.
Melhorias na apresentação da snackbar;

# 1.1.0 (20/11/2024)
Refatoração dos arquivos de estilização da aplicação.
Adição do CHANGELOG.md

# 1.0.3 (14/04/2024)
 Ajustes na responsividade da aplicação. 

# 1.0.2 (14/04/2024)
 Ajuste da responsividade da navbar para dispositivos móveis.

# 1.0.1 (13/04/2024)
 Ajuste nas fontes dos textos;

# 1.0.0 (13/04/2024)
 Publicação da primeira versão;